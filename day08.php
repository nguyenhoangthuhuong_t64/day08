
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
</head>
<style>
    fieldset {
        width: 470px;
        margin: 12%;
        margin-left: 32%;
        border: 1px solid #00AA00;
    }

    .username {
        background-color: #00AA00;
        color: azure;
        padding: 10px 20px;
        margin-right: 25px;
        width: 100px;
        text-align: center;
        border: 1px solid #00AA00
    }

    .text {
        border: 1px solid #00AA00;
        padding: 10px;
        width: 278px;
    }

    .apartment {
        margin-top: 20px;

    }

    .falcuty {
        width: 300px;
        border: 1px solid #00AA00;
    }

    .button {
        margin-left: 40%;
        width: 100px;
        margin-top: 30px;
        height: 30px;
	    text-align: center;
        background-color: #00AA00;
        border: 1px solid #48c76e;
        border-radius: 5px;
        color: black;
        transition-duration: 0.5s;

    }

    .action {
        width: 50px;
        margin-top: 10px;
        height: 30px;
        background-color: #00AA00;
        border: 1px solid #48c76e;
        color: black;
        transition-duration: 0.5s;

    }

    .add {
        width: 70px;
        margin-left: 70%;
        height: 30px;
        background-color: #00AA00;
        border: 1px solid #48c76e;
        border-radius: 5px;
        color: black;
        transition-duration: 0.5s;

    }

    .button:hover {
        opacity: 0.5;
    }

    .style {
        display: flex;
    }

    .text {
        border: 1px solid #00AA00;
        text-align: left;
    }

    .table {
        margin-top: 30px;
        border-collapse: separate;
        border-spacing: 15px 10px;
        width: 100%;
    }

    .search {
        margin-top: 20px;
    }

    .button_clear{
        margin-left: 40%;
        width: 50px;
        margin-top: 30px;
        height: 30px;
        background-color: #00AA00;
        border: 1px solid #48c76e;
        border-radius: 5px;
        color: black;
        transition-duration: 0.5s;

    }
</style>

<body>
    <?php
    session_start();
    $_SESSION = $_POST;
    if (!empty($_POST['search'])) {
        $_SESSION['falcuty'] = isset($_POST['falcuty']) ? $_POST['falcuty'] : '--Falcuty--';
        $_SESSION['key'] = isset($_POST['key']) ? $_POST['key'] : '';
        header("Location: submit.php");
    }

    ?>
    <form method="post" id="myform" action="">
        <fieldset>
            <div class="apartment">
                <div class="style">
                    <div class="username">
                        <label>
                            Phân khoa <sup class="notnull">
                        </label>
                    </div>
                    <select class="falcuty" name="falcuty" id="falcuty">
                        <?php $falcuty = array('' => '', 'MAT' => 'Khoa học máy tính', 'KDL' => 'Khoa học vật liệu');
                        foreach ($falcuty as $key => $value) {
                            echo " <option ";
                            echo isset($_POST['Faculty']) && $_POST['Faculty'] == $key ? " selected " : "";
                            echo " value='" . $key . "'>" . $value . "</option> ";
                        }
                        ?>
                    </select>

                </div>
                <div class="apartment">
                    <div class="style">
                        <div class="username">
                            <label>
                                Từ khóa <sup class="notnull">
                            </label>
                        </div>
                        <input type="text" class="text" id="tukhoa" name="key">
                    </div>
                    </div>
                </div>
            </div class="style">
            <input type="submit" name="clear" class="button_clear" onclick="myFunction()" value="Xóa" />
            <input type="submit" name="search" class="button" value="Tìm kiếm" />

            </div>
            <div>
                <div>
                    <div class="search">
                        <label>
                            Số sinh viên tìm thấy XXX
                        </label>
                        <?php
                        if (isset($_POST["plus"])) {
                            header("Location: day5.php");
                        }
                        ?>

                    </div>
                    <input type="submit" name="add" class="add" value="Thêm" />
                </div>
            </div>
            <div>
                <table class="table">
                <tr>
                        <th> No </th>
                        <th>Tên sinh viên</th>
                        <th>Khoa</th>
                        <th>Action</th>
                    </tr>

                    <tr>
                        <td>1</td>
                        <td>Nguyễn Văn A</td>
                        <td>Khoa học máy tính</td>
                        <td> <input type="submit" name="search" class="action" value="Sửa"> </td>
                        <td> <input type="submit" name="search" class="action" value="Xóa"> </td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>Trần Thị B</td>
                        <td>Khoa học máy tính</td>
                        <td> <input type="submit" name="search" class="action" value="Sửa"> </td>
                        <td> <input type="submit" name="search" class="action" value="Xóa"> </td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td> Nguyễn Hoàng C</td>
                        <td>Khoa học máy tính</td>
                        <td> <input type="submit" name="search" class="action" value="Sửa"> </td>
                        <td> <input type="submit" name="search" class="action" value="Xóa"> </td>

                    </tr>
                    <tr>
                        <td>4</td>
                        <td> Đinh Quang D</td>
                        <td>Khoa học máy tính</td>
                        <td> <input type="submit" name="search" class="action" value="Sửa"> </td>
                        <td> <input type="submit" name="search" class="action" value="Xóa"> </td>


                    </tr>
                </table>
            </div>
            </div>
    </form>
    <script>
        function myFunction() {
            document.getElementById("myForm").reset();
        }
    </script>
    </fieldset>
</body>

</html>
